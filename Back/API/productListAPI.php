<?php

header('Content-Type: application/json');

include_once "../database/UserDAO.php";

/*
 * Returns list of products from database in json format.
 */

$dao = new UserDAO();
$productsFromDatabase = $dao->getProducts();
$productsToReturn = [];

foreach ($productsFromDatabase as $product) {
    $curr = ['sku' => $product->getSKU(),
        'name' => $product->getName(),
        'price'=> $product->getPrice(),
        'type' => $product->getType(),
        'details' => $product->getDetails()];
    array_push($productsToReturn, $curr);
}

echo json_encode($productsToReturn);



