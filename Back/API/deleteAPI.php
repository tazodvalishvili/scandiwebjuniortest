<?php

include_once "../database/UserDAO.php";

/*
 * Gets sku of the product in json format and deletes product from database.
 */

$raw = file_get_contents("php://input");
$data = json_decode($raw, true);
$dao = new UserDAO();
foreach ($data as $productSKU) {
    $dao->deleteProduct($productSKU);
}

