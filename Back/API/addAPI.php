<?php
include_once "../database/UserDAO.php";

/*
 * Gets product data in json format. If data is valid adds product to database. Returns list of errors.
 */

$raw = file_get_contents("php://input");
$data = json_decode($raw, true);
$dao = new UserDAO();

$type = $data[0];
$sku = $data[1];
$name = $data[2];
$price = strval($data[3]);
$details = strval($data[4]);
if(count($data) > 5)
    $details = $details . " " . $data[5] . " " . $data[6];
$product = new Product($sku, $name, $price, $type, $details);
$errors = $dao->addProduct($product);
echo json_encode($errors);
