<?php
Class DBConnection {
    private string $hostname;
    private string $username;
    private string $password;
    private string $database;

    public function __construct() {
        $this->hostname = "localhost";
        $this->username = "root";
        $this->password = "";
        $this->database = "localhostdb";
    }

    /*
     * Returns connection.
     */
    public function getConnection() {
        $connection = mysqli_connect($this->hostname, $this->username, $this->password, $this->database);
        if(!$connection)
            die("Connection failed") . mysqli_error($connection);
        return $connection;
    }
}
