<?php
include_once "DAO.php";
include_once "DBConnection.php";
include_once "../Classes/Product.php";
include_once "../Classes/Book.php";
include_once "../Classes/DVD.php";
include_once "../Classes/Furniture.php";
include_once "../Classes/Validator.php";

class UserDAO implements DAO {
    private $connection;

    public function __construct() {
        $dbconnection = new DBConnection();
        $this->connection = $dbconnection->getConnection();
    }

    function getProducts(): array
    {
        $query = "SELECT * FROM products";
        $productsRaw = mysqli_query($this->connection, $query);
        $products = [];
        while ($arr = mysqli_fetch_assoc($productsRaw)) {
            $sku = $arr["SKU"];
            $name = $arr["NAME"];
            $price = $arr["PRICE"];
            $type = $arr["TYPE"];
            $details = $arr["DETAILS"];
            $curr_product = new $type($sku, $name, $price, $type, $details);
            $products[] = $curr_product;
        }
        return $products;
    }

    function addProduct($product) {
        $validator = new Validator($product, $this);
        $errors = $validator->validate();
        if($validator->getErrorCount() == 0) {
            $sku = $product->getSKU();
            $name = $product->getName();
            $price = $product->getPrice();
            $type = $product->getType();
            $details = $product->getDetails();
            $query = "INSERT INTO `products` (`SKU`, `NAME`, `PRICE`, `TYPE`, `DETAILS`) VALUES ('$sku', '$name', '$price', '$type', '$details')";
            mysqli_query($this->connection, $query);
        }
        return $errors;
    }


    function deleteProduct($sku) {
        $query = "DELETE FROM products WHERE SKU = '$sku'";
        mysqli_query($this->connection, $query);
    }

    function containsSku($sku): bool
    {
        $query = "SELECT COUNT(*) AS RESULT FROM products WHERE SKU = '$sku'";
        $result = mysqli_query($this->connection, $query);
        $arr = mysqli_fetch_assoc($result);
        $count = $arr["RESULT"];
        if($count == 0)
            return false;
        return true;
    }
}