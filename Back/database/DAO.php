<?php

interface DAO {
    /*
     * Returns products from database.
     */
    function getProducts();

    /*
     * Adds product to database.
     */
    function addProduct($product);

    /*
     * Deletes product from database.
     */
    function deleteProduct($sku);

    /*
     * Checks if database contains following sku.
     */
    function containsSku($sku);
}