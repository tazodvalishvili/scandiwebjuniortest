<?php

include_once "../database/DAO.php";
include_once "../database/DBConnection.php";
include_once "../database/UserDAO.php";
include_once "Product.php";
include_once "Book.php";
include_once "DVD.php";
include_once "Furniture.php";

const SKU_LENGTH_LIMIT = 30;
const NAME_LENGTH_LIMIT = 50;
const DETAILS_LENGTH_LIMIT = 25;
const TYPES = ["DVD", "Book", "Furniture"];

class Validator {
    private $product;
    private $errors;
    private $errorCount;
    private $dao;

    public function __construct($product, $dao) {
        $this->errorCount = 0;
        $this->errors = ["sku" => true, "name" => true, "price" => true, "type" => true, "details" => true];
        $this->product = $product;
        $this->dao = $dao;
    }

    /*
     * Checks if input is empty.
     */
    private function isEmpty($input): bool
    {
        if(empty(trim($input)))
            return true;
        return false;
    }

    /*
     * Returns number of errors.
     */
    function getErrorCount(): int
    {
        return $this->errorCount;
    }

    /*
     * Checks if input length is valid.
     */
    private function lengthIsValid($input, $limit): bool
    {
        if(strlen($input) <= $limit)
            return true;
        return false;
    }

    /*
     * Checks if sku is valid.
     */
    function skuValidation() {
        $sku = $this->product->getSKU();
        if($this->isEmpty($sku)) {
            $this->errors["sku"] = "SKU field must not be empty.";
            $this->errorCount++;
        }
        else if(!$this->lengthIsValid($sku, SKU_LENGTH_LIMIT)) {
            $this->errors["sku"] = "SKU is too long.";
            $this->errorCount++;
        }
        else if($this->dao->containsSku($sku)) {
            $this->errors["sku"] = "SKU already exists.";
            $this->errorCount++;
        }
    }

    /*
     * Checks if name is valid.
     */
    function nameValidation() {
        $name = $this->product->getName();
        if($this->isEmpty($name)) {
            $this->errors["name"] = "Name field must not be empty.";
            $this->errorCount++;
        }
        else if(!$this->lengthIsValid($name, NAME_LENGTH_LIMIT)) {
            $this->errors["name"] = "Name is too long.";
            $this->errorCount++;
        }
    }

    /*
     * Checks if price is valid.
     */
    function priceValidation() {
        $price = substr($this->product->getPrice(), 0, -1);
        if($this->isEmpty($price)) {
            $this->errors["price"] = "Price field must not be empty.";
            $this->errorCount++;
        }
        else if(!is_numeric($price)) {
            $this->errors["price"] = "Price must be a numeric input.";
            $this->errorCount++;
        }
    }

    /*
     * Checks if type is valid.
     */
    function typeValidation() {
        $type = $this->product->getType();
        if(!in_array($type, TYPES)) {
            $this->errors["type"] = "Type must be one of these: [DVD, Book, Furniture]";
            $this->errorCount++;
        }
    }

    /*
     * Checks if details are valid.
     */
    function detailsValidation() {
        $details = $this->product->getDetails();
        if($this->isEmpty($details)) {
            $this->errors["details"] = "Details field must not be empty.";
            $this->errorCount++;
        }
        else if(!$this->lengthIsValid($details, DETAILS_LENGTH_LIMIT)) {
            $this->errors["details"] = "Details is too long.";
            $this->errorCount++;
        }
        else if (!is_numeric(str_replace(' ', '', $details))) {
            $this->errors["details"] = "Details must be a numeric type.";
            $this->errorCount++;
        }
    }

    /*
     * Runs validation process for all the data and returns errors.
     */
    function validate(): array
    {
        $this->skuValidation();
        $this->nameValidation();
        $this->priceValidation();
        $this->typeValidation();
        $this->detailsValidation();
        return $this->errors;
    }
}