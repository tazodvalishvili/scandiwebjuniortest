<?php

class Product {
    private $sku;
    private $name;
    private $price;
    private $type;
    private $details;

    public function __construct($sku, $name, $price, $type, $details) {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price . '$';
        $this->type = $type;
        $this->details = $details;
    }

    /*
     * Returns sku of the product.
     */
    function getSKU() {
        return $this->sku;
    }

    /*
     * Returns name of the product.
     */
    function getName() {
        return $this->name;
    }

    /*
     * Returns price of the product.
     */
    function getPrice() {
        return $this->price;
    }

    /*
     * Returns type of the product.
     */
    function getType() {
        return $this->type;
    }

    /*
     * Returns details of the product.
     */
    function getDetails() {
        return $this->details;
    }
}