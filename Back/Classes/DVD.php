<?php

class DVD extends Product {

    public function __construct($sku, $name, $price, $type, $details)
    {
        parent::__construct($sku, $name, $price, $type, $details);
    }

    function getDetails()
    {
        return "Size: " . parent::getDetails() . "MB";
    }
}