<?php

class Furniture extends Product {

    public function __construct($sku, $name, $price, $type, $details)
    {
        parent::__construct($sku, $name, $price, $type, $details);
    }

    function getDetails()
    {
        return "Dimension: " . str_replace(' ', 'x', parent::getDetails());
    }

}