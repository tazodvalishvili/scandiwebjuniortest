<?php

class Book extends Product {

    public function __construct($sku, $name, $price, $type, $details)
    {
        parent::__construct($sku, $name, $price, $type, $details);
    }

    function getDetails()
    {
        return "Weight: " . parent::getDetails() . "KG";
    }
}