$(function(){
    /*
        Shows different details input fields for different types.
     */
    $("#productType" ).change(function() {
        let selectedType = document.querySelector("#productType").value;
        if(selectedType === "TypeSwitcher") {
            $("#empty-product").html("<p id=\"product-type-empty\" style=\"color: forestgreen\"><strong> <br> Please select product type</strong></p>");
        } else if(selectedType === "DVD") {
            $("#empty-product").html(" <div class=\"form-group row\" id=\"dvd-form\">\n" +
                "        <label class=\"col-sm-2 col-form-label\">Size(MB):</label>\n" +
                "        <div class=\"col-sm-10\">\n" +
                "            <input class=\"form-control\" id=\"size\" name=\"product\" placeholder=\"Size(MB)\">\n" +
                "        </div>\n" +
                "    </div>\n" +
                "\n" +
                "    <p style=\"color: forestgreen\"><strong> <br>Please, provide size</strong></p>");
        } else if(selectedType === "Book") {
            $("#empty-product").html("<div class=\"form-group row\" id=\"book-form\">\n" +
                "        <label class=\"col-sm-2 col-form-label\">Weight(KG):</label>\n" +
                "        <div class=\"col-sm-10\">\n" +
                "            <input class=\"form-control\" id=\"weight\" name=\"product\" placeholder=\"Weight(KG)\">\n" +
                "        </div>\n" +
                "    </div>\n" +
                "\n" +
                "    <p style=\"color: forestgreen\"><strong> <br>Please, provide weight</strong></p>");
        } else if(selectedType === "Furniture") {
            $("#empty-product").html("    <div class=\"form-group row\" id=\"furniture-form-height\">\n" +
                "        <label class=\"col-sm-2 col-form-label\">Height(CM):</label>\n" +
                "        <div class=\"col-sm-10\">\n" +
                "            <input class=\"form-control\" id=\"height\" name=\"product\" placeholder=\"Height(CM)\">\n" +
                "        </div>\n" +
                "    </div>\n" +
                "\n" +
                "    <div class=\"form-group row\" id=\"furniture-form-width\">\n" +
                "        <label class=\"col-sm-2 col-form-label\">Width(CM):</label>\n" +
                "        <div class=\"col-sm-10\">\n" +
                "            <input class=\"form-control\" id=\"width\" name=\"product\" placeholder=\"Width(CM)\">\n" +
                "        </div>\n" +
                "    </div>\n" +
                "\n" +
                "    <div class=\"form-group row\" id=\"furniture-form-length\">\n" +
                "        <label class=\"col-sm-2 col-form-label\">Length(CM):</label>\n" +
                "        <div class=\"col-sm-10\">\n" +
                "            <input class=\"form-control\" id=\"length\" name=\"product\" placeholder=\"Length(CM)\">\n" +
                "        </div>\n" +
                "    </div>\n" +
                "\n" +
                "    <p style=\"color: forestgreen\"><strong> <br>Please, provide dimensions</strong></p>");
        }
    })

    /*
        Sends post request to the server to add the product with following inputs. If errors occur while validation
        shows the client them, else adds product and returns to the main page.
     */
    $("#add-btn").click(function (){
        let inputs = [];
        inputs.push($("#productType" ).val());
        $.each($("input[name='product']:text"), function(){
            inputs.push($(this).val());
        });
        $.ajax({
            type: "POST",
            url: "http://localhost/ScandiwebJuniorTest/Back/API/addAPI.php",
            data: JSON.stringify(inputs),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                let errorCount = 0;
                let skuCheck = data["sku"];
                let nameCheck = data["name"];
                let priceCheck = data["price"];
                let detailsCheck = data["details"];
                let error = "";
                $(".error-list").empty();
                if(skuCheck !== true) {
                    error = "<p class='text-danger'><strong> " + skuCheck + " </strong></p> ";
                    $(".error-list").append(error);
                    errorCount++;
                }
                if(nameCheck !== true) {
                    error = "<p class='text-danger'><strong> " + nameCheck + " </strong></p> ";
                    $(".error-list").append(error);
                    errorCount++;
                }
                if(priceCheck !== true) {
                    error = "<p class='text-danger'><strong> " + priceCheck + " </strong></p> ";
                    $(".error-list").append(error);
                    errorCount++;
                }
                if(detailsCheck !== true) {
                    error = "<p class='text-danger'><strong> " + detailsCheck + " </strong></p> ";
                    $(".error-list").append(error);
                    errorCount++;
                }
                if(errorCount === 0)
                    location.href = "ProductListPage.html";
            }
        });

    });

    /*
        Returns to the main page.
     */
    $("#cancel-btn").click(function (){
        location.href = "ProductListPage.html";
    });


});