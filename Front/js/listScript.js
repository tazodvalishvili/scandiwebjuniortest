$(function(){
    /*
        Sends get request to get products list and shows all the products on the main page.
     */
    $.get("http://localhost/ScandiwebJuniorTest/Back/API/productListAPI.php", function (data) {
        for(let i = 0; i < data.length; i++) {
            let currentProductHTML = '';
            currentProductHTML += ' <div class="col mb-4">';
            currentProductHTML += ' <div class="card">';
            currentProductHTML += ' <div class="card-body">';
            currentProductHTML += ' <input type="checkbox" class="delete-checkbox" name="products" value= "' + data[i].sku + '">';
            currentProductHTML += ' <h6 class="card-text">'+ data[i].sku +'</h6>'
            currentProductHTML += ' <h6 class="card-text">'+ data[i].name +'</h6>'
            currentProductHTML += ' <h6 class="card-text">'+ data[i].price +'</h6>'
            currentProductHTML += ' <h6 class="card-text">'+ data[i].details +'</h6>'
            currentProductHTML += ' </div>';
            currentProductHTML += ' </div>';
            currentProductHTML += ' </div>';
            $(".product-place").append(currentProductHTML);
        }
    });

    /*
        Goes to the add product page.
     */
    $("#add-product-btn").click(function (){
        location.href = "AddProductPage.html";
    });

    /*
        Sends post request to the server to delete checked products.
     */
    $("#delete-product-btn").click(function (){
        let checkedProducts = [];
        $.each($("input[name='products']:checked"), function(){
            checkedProducts.push($(this).val());
        });
        if(checkedProducts.length === 0)
            return;
        $.ajax({
            type: "POST",
            url: "http://localhost/ScandiwebJuniorTest/Back/API/deleteAPI.php",
            data: JSON.stringify(checkedProducts),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        });
        location.reload();
    });
});